import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_elogd_enabled_and_started(Service):
    elogd = Service('elogd')
    assert elogd.is_enabled
    assert elogd.is_running


def test_elog_homepage(Command):
    cmd = Command('curl -Lk https://localhost')
    assert '<title>ELOG demo</title>' in cmd.stdout


def test_imaemagick_installed(Command):
    cmd = Command('convert --version')
    assert 'Version: ImageMagick' in cmd.stdout


def test_theme_installed(File):
    assert File('/usr/local/elog/themes/Confluence/default.css').exists
