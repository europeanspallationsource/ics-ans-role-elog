ics-ans-role-elog
=================

Ansible role to install ELOG.

ELOG is an Electronic Logbook.
See https://midas.psi.ch/elog/index.html for more information.

TODO: add a custom elogd.cfg file

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
elog_rpm: "http://midas.psi.ch/elog/download/RPMS/elog-latest.i386.rpm"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-elog
```

License
-------

BSD 2-clause
